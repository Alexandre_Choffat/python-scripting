#!/usr/bin/env python3

import csv,sqlite3, sys

def ciqual(csvfile):
    with open(csvfile, newline='') as ciqual :
        #Ouverture de la base de donnée.
        sql = sqlite3.connect("ciqual.db")
        c = sql.cursor()
        #Ouverture du fichier csv.
        ciqualreader = csv.reader(ciqual)
        #Première ligne
         # Initialisation des tables.
        c.execute('create table nutrient ( id integer primary key autoincrement, name text not null);')
        c.execute('create table grp ( id text unique not null, name text not null);')
        c.execute('create table ssgrp ( id text unique not null, name text not null, grp_id text not null);')
        c.execute('create table ssssgrp( id text unique not null, name text not null, ssgrp_id text not null);')
        c.execute('create table food ( id text not null, name text not null, grp_id text not null, ssgrp_id text not null, ssssgrp_id text not null);')
        c.execute('create table nutdata (id integer primary key autoincrement, food_id text not null ,value text, nutrient_id integer );')
        #Mémorisation des codes pour ne pas ajouter deux fois les groupes.
        last_grp_code = "00"
        last_ssgrp_code = "0000"
        last_ssssgrp_code = "000000"
        #On initalise les groupes/ssgroupes nuls.
        c.execute("insert into grp (id,name) values('00','-');")
        c.execute("insert into ssgrp (id,name,grp_id) values ('0000','-','00');")
        c.execute("insert into ssssgrp (id,name,ssgrp_id) values ('000000','-','0000');")
        first_row = True
        for row in ciqualreader :
            if first_row :
                #Remplissage de la table des nutriments.
                for name_nutr in row[9:]:
                    c.execute('insert into nutrient (name) values (?);',(name_nutr,))
                first_row = False
            else :
                #Remplissage des autres tables.
                grp_code,ssgrp_code,ssssgrp_code,grp_nom,ssgrp_nom,ssssgrp_nom,alim_code,alim_nom = row[:8]
                if grp_code != last_grp_code and grp_code != "00":
                    c.execute('insert into grp (id,name) values(?,?);',(grp_code,grp_nom))
                if (ssgrp_code != last_ssgrp_code and ssgrp_code != "0000") :
                    c.execute('insert into ssgrp (id,name,grp_id) values (?,?,?);',(ssgrp_code,ssgrp_nom,grp_nom))
                if (ssssgrp_code != last_ssssgrp_code and ssssgrp_code != "000000"):
                    c.execute('insert into ssssgrp (id,name,ssgrp_id) values (?,?,?);',(ssssgrp_code,ssssgrp_nom,ssgrp_nom))
                c.execute('insert into food (grp_id,ssgrp_id,ssssgrp_id,id,name) values (?,?,?,?,?);',(grp_code,ssgrp_code,ssssgrp_code,alim_code,alim_nom))
                nut_id = 1
                for nut_data in row[9:]:
                    c.execute('insert into nutdata (food_id,value,nutrient_id) values (?,?,?);',(alim_code,nut_data,nut_id))
                    nut_id += 1
                last_grp_code = grp_code
                last_ssgrp_code = ssgrp_code
                last_ssssgrp_code = ssssgrp_code

csvfile = sys.argv[1]
ciqual(csvfile)