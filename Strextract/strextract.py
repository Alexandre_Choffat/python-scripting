#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, argparse, re

parser = argparse.ArgumentParser()
parser.add_argument("dir" type = str, required = True, help = 'directory name' )
parser.add_argument("-suffix", type=str, action = 'store_true', help ='only consider files that end with suffix')
parser.add_argument("-path", action = 'store_true' , help ='every line starts with the file path associated with the line.')
parser.add_argument("-a","--all", action = 'store_true', help = 'operate on all files including hidden files.')
args = parser.parse_args()

def strextract(dir):
    compiled_regex = re.compile('\".+\"|\'.+\'')
    #Exploring directory
    for (dirpath, _, filenames) in os.walk(dir):
    #Exploring files
        for filename in filenames:
            if args.all or not filename.startswith('.') :
                if not args.suffix or filename.endswith(suffix):
                    try :
                        open(filename,'r') as file :
                        for line in file:
                            if args.path :
                                print(f'{os.path.join(dirpath, filename)}\t')
                            for match in compiled_regex.finditer(line)
                                print(match)
                    except Exception as E :
                        print(E) 


strextract(args.dir)