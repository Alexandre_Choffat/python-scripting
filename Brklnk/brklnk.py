#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests, argparse
from urllib.parse import urlparse
from urllib.parse import urljoin
from bs4 import BeautifulSoup

def brknlnk(url, depth, visited_links = []):
    #Condition de terminaison
    if depth >= 0 :
        #Décomposition du code html
        r = requests.get(url)
        visited_links.append(url)
        if r.status_code >= 400 :
            print(url)
        else :
            soup = BeautifulSoup(r.text, 'lxml')
            for link in soup.find_all('a'):
                #On regarde si le lien est complet.
                linkparse = urlparse(link)
                #S'il ne l'est pas on le complète.
                if linkparse.scheme == '' or linkparse.netloc == '' :
                    link = urljoin(page_web, link)
                #Vérification que link n'a pas déjà été visité.
                if link not in visited_links
                    brknlnk(link, depth-1, visited_links)
    

parser = argparse.ArgumentParser()
parser.add_argument("page_web", type = str, help = 'page web which we want to know the broken links of')
parser.add_argument("-depth", type = int, default = 1, help = 'depth of link searchs')
args = parser.parse_args()
brknlnk(args.page_web, args.depth)